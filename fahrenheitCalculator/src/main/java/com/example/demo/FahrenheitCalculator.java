package com.example.demo;

public class FahrenheitCalculator {
    public double getFahrenheit(double celsius) {
        return celsius * 1.8 + 32;
    }
}
