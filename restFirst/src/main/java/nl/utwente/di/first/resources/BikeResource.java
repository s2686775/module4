package nl.utwente.di.first.resources;

import nl.utwente.di.first.dao.BikeDao;
import nl.utwente.di.first.model.Bike;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.xml.bind.JAXBElement;

public class BikeResource {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;

    public BikeResource(UriInfo uriInfo, Request request) {
        this.uriInfo = uriInfo;
        this.request = request;
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Bike getBike(@PathParam("id") String id) {
        Bike bike = BikeDao.instance.getModel().get(id);
        if(bike == null)
            throw new RuntimeException("Get: Bike with " + id +  " not found");
        return bike;
    }

    @GET
    @Produces(MediaType.TEXT_XML)
    public Bike getBikeHTML(@PathParam("id") String id) {
        Bike bike = BikeDao.instance.getModel().get(id);
        if(bike == null)
            throw new RuntimeException("Get: Bike with " + id +  " not found");
        return bike;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public Response updateBike(JAXBElement<Bike> bike) {
        Bike b = bike.getValue();
        return updateAndResponse(b);
    }

    private Response updateAndResponse(Bike bike) {
        Response response;
        if(BikeDao.instance.getModel().containsKey(bike.getId())) {
            response = Response.noContent().build();
        } else {
            response = Response.created(uriInfo.getAbsolutePath()).build();
        }
        BikeDao.instance.getModel().put(bike.getId(), bike);
        return response;
    }

    @DELETE
    public void deleteBike(@PathParam("id") String id) {
        Bike bike = BikeDao.instance.getModel().remove(id);
        if(bike == null)
            throw new RuntimeException("Delete: Bike with " + id +  " not found");
    }
}