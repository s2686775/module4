package nl.utwente.di.first.dao;

import nl.utwente.di.first.model.Bike;

import java.util.HashMap;
import java.util.Map;

public enum BikeDao {
    instance;

    private Map<String, Bike> contentProvider = new HashMap<>();

    BikeDao() {

    }

    public void delete(Bike bike) {
        contentProvider.remove(bike);
    }

    public Map<String, Bike> getModel() {
        return contentProvider;
    }
}
