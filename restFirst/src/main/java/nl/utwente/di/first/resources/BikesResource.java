package nl.utwente.di.first.resources;

import nl.utwente.di.first.dao.BikeDao;
import nl.utwente.di.first.model.Bike;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.xml.bind.JAXBElement;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


@Path("/bikes")
public class BikesResource {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;
    Map<Bike, Boolean> orderedBike = new HashMap<>();

    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes(MediaType.APPLICATION_XML)
    public Response createBike(JAXBElement<Bike> bike) {
        Bike bikeValue = bike.getValue();
        //servletResponse.sendRedirect("../create_bike.html");
        return createAndGetResponse(bikeValue);
    }

    private Response createAndGetResponse(Bike bike) {
        Response res;
        if (BikeDao.instance.getModel().containsKey(bike.getId())) {
            res = Response.status(409).entity(bike).build();
        } else {
            res = Response.ok(bike).build();
        }
        BikeDao.instance.getModel().put(bike.getId(), bike);
        return res;
    }

    //todo modify filter, getallbikes in one method,
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Bike> getBikes(@QueryParam("color") String color, @QueryParam("gender") String gender) {
        Map<String, Bike> bikesMap = BikeDao.instance.getModel();
        ArrayList<Bike> arrayListBikes = new ArrayList<>();
        if (color != null) {
            bikesMap.forEach((s, bike) -> {
                if (bike.getColour().equalsIgnoreCase(color))
                    arrayListBikes.add(bike);
            });
        }
        if (gender != null) {
            bikesMap.forEach((s, bike) -> {
                if (bike.getGender().equalsIgnoreCase(gender))
                    arrayListBikes.add(bike);
            });
        }
        if (!arrayListBikes.isEmpty())
            return arrayListBikes;
        //color == null || gender == null
        return new ArrayList<>(bikesMap.values());
    }

    @GET
    @Path("/{bikeId}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Bike getBikeDetails(@PathParam("bikeId") String id) {
        return BikeDao.instance.getModel().get(id);
    }


    @GET
    @Path("/{bikeId}/order")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public String orderBike(@PathParam("bikeId") String id) {
        Bike bikeToOrder = BikeDao.instance.getModel().get(id);
        if (bikeToOrder != null && !orderedBike.containsKey(bikeToOrder))
            orderedBike.put(bikeToOrder, false);
        if (bikeToOrder != null) {
            if (!orderedBike.get(bikeToOrder)) {
                orderedBike.put(bikeToOrder, true);
                return "The bike has been ordered";
            }
            return "You cannot order this bike anymore!";
        }
        return "The bike does not exist!";
    }

    @PUT
    @Path("/{bikeId}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void updateBike(@PathParam("bikeId") String id){
        
    }

    @DELETE
    @Path("/{bikeId}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void deleteBike(@PathParam("bikeId") String id) {
        Bike bike = BikeDao.instance.getModel().remove(id);
        if(bike == null)
            throw new RuntimeException("Delete: Bike with " + id +  " not found");
    }
}