package nl.utwente.di.first.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Bike {
    private String id, ownerName, colour, gender;

    public Bike(String id, String ownerName, String colour, String gender) {
        this.id = id;
        this.ownerName = ownerName;
        this.colour = colour;
        this.gender = gender;
    }


    public Bike() {

    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getColour() {
        return colour;
    }

    public String getGender() {
        return gender;
    }

    public String getId() {
        return id;
    }

    public String getOwnerName() {
        return ownerName;
    }
}
