package nl.utwente.di.bookQuote;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class TestQuoter {
    @Test
    public void testBook1() throws Exception {
        Quoter quoter = new Quoter();
        double price = quoter.getBookPrice("9");
        Assertions.assertEquals(0.0, price, 0.0, "Price of book 1");
    }
}
